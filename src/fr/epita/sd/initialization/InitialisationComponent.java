package fr.epita.sd.initialization;

import java.io.IOException;

import javax.annotation.PostConstruct;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.dataformat.xml.XmlMapper;
import com.google.gson.Gson;

import fr.epita.sd.feature.quiz.Quiz;
import fr.epita.sd.feature.quiz.QuizService;

@Component
public class InitialisationComponent {
	
	@Autowired
	private QuizService quizService;
	
	@PostConstruct
	private void postConstruct() throws JsonParseException, JsonMappingException, IOException {
		//loadFromXml();
		//<loadFromJson();
	}

	private void loadFromXml() throws IOException, JsonParseException, JsonMappingException {
		String xml = "<Quiz title=\"Structured Data Quiz\">\r\n" + 
				"	<questions>\r\n" + 
				"		<Question text=\"What is the first step of the DIKW chain?\">\r\n" + 
				"			<answers>\r\n" + 
				"				<Answer text=\"Data\" validity=\"true\"></Answer>\r\n" + 
				"				<Answer text=\"Information\" validity=\"false\"></Answer>\r\n" + 
				"				<Answer text=\"Knowledge\" validity=\"false\"></Answer>\r\n" + 
				"				<Answer text=\"Wisdom\" validity=\"false\"></Answer>\r\n" + 
				"			</answers>\r\n" + 
				"		</Question>\r\n" + 
				"		<Question text=\"What is the last step of the DIKW chain?\">\r\n" + 
				"			<answers>\r\n" + 
				"				<Answer text=\"Data\" validity=\"false\"></Answer>\r\n" + 
				"				<Answer text=\"Information\" validity=\"false\"></Answer>\r\n" + 
				"				<Answer text=\"Knowledge\" validity=\"false\"></Answer>\r\n" + 
				"				<Answer text=\"Wisdom\" validity=\"true\"></Answer>\r\n" + 
				"			</answers>\r\n" + 
				"		</Question>\r\n" + 
				"	</questions>\r\n" + 
				"</Quiz>";
		
		XmlMapper mapper = new XmlMapper();
		Quiz quiz = mapper.readValue(xml, Quiz.class);
		quizService.save(quiz);
	}

	private void loadFromJson() {
		String json = "{\r\n" + 
				"	\"title\": \"Structured Data Quiz\",\r\n" + 
				"	\"questions\": [\r\n" + 
				"		{\r\n" + 
				"			\"text\": \"What is the first step of the DIKW chain?\",\r\n" + 
				"			\"answers\": [\r\n" + 
				"				{\r\n" + 
				"					\"text\": \"Data\",\r\n" + 
				"					\"validity\": true\r\n" + 
				"				},\r\n" + 
				"				{\r\n" + 
				"					\"text\": \"Information\",\r\n" + 
				"					\"validity\": false\r\n" + 
				"				},\r\n" + 
				"				{\r\n" + 
				"					\"text\": \"Knowledge\",\r\n" + 
				"					\"validity\": false\r\n" + 
				"				},\r\n" + 
				"				{\r\n" + 
				"					\"text\": \"Wisdom\",\r\n" + 
				"					\"validity\": false\r\n" + 
				"				}\r\n" + 
				"			]\r\n" + 
				"		},\r\n" + 
				"		{\r\n" + 
				"			\"text\": \"What is the last step of the DIKW chain?\",\r\n" + 
				"			\"answers\": [\r\n" + 
				"				{\r\n" + 
				"					\"text\": \"Data\",\r\n" + 
				"					\"validity\": false\r\n" + 
				"				},\r\n" + 
				"				{\r\n" + 
				"					\"text\": \"Information\",\r\n" + 
				"					\"validity\": false\r\n" + 
				"				},\r\n" + 
				"				{\r\n" + 
				"					\"text\": \"Knowledge\",\r\n" + 
				"					\"validity\": false\r\n" + 
				"				},\r\n" + 
				"				{\r\n" + 
				"					\"text\": \"Wisdom\",\r\n" + 
				"					\"validity\": true\r\n" + 
				"				}\r\n" + 
				"			]\r\n" + 
				"		}\r\n" + 
				"	]\r\n" + 
				"}";
		
		Gson gson = new Gson();
		Quiz quiz = gson.fromJson(json, Quiz.class);
		quizService.save(quiz);
	}
	
	
}

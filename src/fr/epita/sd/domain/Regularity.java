package fr.epita.sd.domain;

public class Regularity {

	private Integer year;
	private Integer month;
	private Double compositeRegularityPercentage;
	private Double punctualityOriginPercentage;
	
	public Integer getYear() {
		return year;
	}
	public void setYear(Integer year) {
		this.year = year;
	}
	public Integer getMonth() {
		return month;
	}
	public void setMonth(Integer month) {
		this.month = month;
	}
	public Double getCompositeRegularityPercentage() {
		return compositeRegularityPercentage;
	}
	public void setCompositeRegularityPercentage(Double compositeRegularityPercentage) {
		this.compositeRegularityPercentage = compositeRegularityPercentage;
	}
	public Double getPunctualityOriginPercentage() {
		return punctualityOriginPercentage;
	}
	public void setPunctualityOriginPercentage(Double punctualityOriginPercentage) {
		this.punctualityOriginPercentage = punctualityOriginPercentage;
	}
}

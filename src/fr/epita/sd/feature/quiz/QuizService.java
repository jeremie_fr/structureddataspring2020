package fr.epita.sd.feature.quiz;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import fr.epita.sd.feature.quiz.answer.Answer;
import fr.epita.sd.feature.quiz.question.Question;

@Service
public class QuizService {

	@Autowired
	private QuizRepository repository;
	
	public Quiz save(Quiz quiz) {
		
		for (Question question : quiz.getQuestions()) {
			question.setQuiz(quiz);
			for (Answer answer : question.getAnswers()) {
				answer.setQuestion(question);
			}
		}
		
		return repository.save(quiz);
	}
	
	public Quiz getById(Long id) {
		return repository.findById(id).get();
	}
	
	public List<Quiz> getAll()  {
		return repository.findAll();
	}
}
